import java.util.Scanner;

public class ContaBancaria {

    Scanner sc = new Scanner(System.in);
    int numero = 123;
    double saldo = 500;
    String titular = "Pessoa";
    double n;

    public void depositar() {
        System.out.println("Digite o valor do deposito: ");
        n = sc.nextDouble();
        saldo = saldo + n;
    }

    public void sacar() {
        System.out.println("Digite o valor do saque: ");
        n = sc.nextDouble();
        saldo = saldo - n;
    }

    public void imprimir() {
        System.out.printf("Saldo: %.2f", saldo);
    }
}
