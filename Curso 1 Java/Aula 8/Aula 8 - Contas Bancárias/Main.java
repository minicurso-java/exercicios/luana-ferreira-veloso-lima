import java.util.Scanner;

public class Main extends ContaBancaria {
    public static void main(String[] args) {

        int opt;

        Scanner sc = new Scanner(System.in);
        ContaBancaria conta = new ContaBancaria();

        System.out.println("Titular da conta: " + conta.titular);
        System.out.println("Número: " + conta.numero);

        System.out.println("1.Sacar\n2.Depositar\n3.Verificar saldo\n4.Sair");
        opt = sc.nextInt();

        while (opt >= 1 && opt <= 4) {
            if (opt == 1) {
                conta.sacar();
            }
            else if (opt == 2) {
                conta.depositar();
            }
            else if (opt == 3) {
                conta.imprimir();
            }
            else {
                break;
            }
            opt = sc.nextInt();
        }
    }
}