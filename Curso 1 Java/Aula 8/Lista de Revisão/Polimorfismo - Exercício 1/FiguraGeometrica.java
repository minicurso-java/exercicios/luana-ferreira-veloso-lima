interface FiguraGeometrica {

    double calcularArea();
    double calcularPerimetro();
}
