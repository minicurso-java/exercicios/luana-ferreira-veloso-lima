import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int opt;

        Quadrado q = new Quadrado();
        Circulo c = new Circulo();
        Triangulo t = new Triangulo();

        Scanner sc = new Scanner(System.in);
        System.out.println("Escolha uma figura geométrica");
        System.out.println("1.Quadado\n2.Círculo\n3.Triangulo\n4.Sair");
        opt = sc.nextInt();

        while (opt >= 1 && opt < 5) {

            switch (opt) {

                case 1:
                    System.out.println("Quadrado");
                    System.out.println("Área: " + q.calcularArea());
                    System.out.println("Perímetro: " + q.calcularPerimetro());
                    break;

                case 2:
                    System.out.println("Círculo");
                    System.out.printf("Área: %.2f", c.calcularArea());
                    System.out.printf("\nPerimetro: %.2f", c.calcularPerimetro());
                    System.out.println(" ");
                    break;

                case 3:
                    System.out.println("Triangulo");
                    System.out.println("Área: " + t.calcularArea());
                    System.out.println("Perimetro: " + t.calcularPerimetro());
                    break;

                case 4:
                    break;
            }

            System.out.println("Escolha outra figura geométrica ou 4 para sair");
            opt = sc.nextInt();
            if (opt == 4)
                break;

        }

    }

}