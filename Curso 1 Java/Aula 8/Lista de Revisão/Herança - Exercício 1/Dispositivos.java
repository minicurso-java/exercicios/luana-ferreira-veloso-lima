class Smartphone extends DispositivosEletronicos {

    public void ligar() {
        System.out.println("Ligando...");
    }
    public void mandarMensagens() {
        System.out.println("Mandando mensagem...");
    }

    String marca = " Samsung";
    String dataFabricacao = "Fabricado em: 10/12/2018";

}

class Tablet extends DispositivosEletronicos {

    String marca = " Apple";
    String dataFabricacao = "Fabricado em: 07/07/2017";

    public void ligarFaceApp() {
        System.out.println("Ligando Face App...");
    }
    public void assistirStreaming() {
        System.out.println("Assistindo Streaming");
    }

}
class Notebook extends DispositivosEletronicos {

    String marca = "Asus";
    String dataFabricacao = "Fabricado em: 14/08/2022";

    public void jogarAAA() {
        System.out.println("Jogando...");
    }
    public void codar() {
        System.out.println("Codando...");
    }

}

