import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int opt;

        Smartphone s = new Smartphone();
        Tablet t = new Tablet();
        Notebook n = new Notebook();

        Scanner sc = new Scanner(System.in);
        System.out.println("Escolha um dispositivo:");
        System.out.println("1.Smartphone\n2.Tablet\n3.Notebook\n4.Sair");
        opt = sc.nextInt();

        while (opt >= 1 && opt < 5) {

            switch (opt) {

                case 1:
                    System.out.print("Smartphone");
                    System.out.println(s.marca + "\n" + s.dataFabricacao);
                    s.ligar();
                    s.mandarMensagens();
                    break;

                case 2:
                    System.out.print("Tablet");
                    System.out.println(t.marca + "\n" + t.dataFabricacao);;
                    t.ligarFaceApp();
                    t.mandarMensagens();
                    break;

                case 3:
                    System.out.print("Notebook");
                    System.out.println(n.marca + "\n" + n.dataFabricacao);
                    n.jogarAAA();
                    n.codar();
                    break;

                case 4:
                    break;
            }

            System.out.println("Escolha outro dispositivo ou 4 para sair");
            opt = sc.nextInt();
            if (opt == 4)
                break;

        }

    }

}