import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int opt;

        Scanner sc = new Scanner(System.in);
        Livros livros = new Livros();
        Eletronicos eletronicos = new Eletronicos();
        Roupas roupas = new Roupas();

        System.out.println("Bem vindo(a) ao mercado virtual!");
        System.out.println("Escolha uma categoria: \n1.Livros\n2.Eletrônicos\n3.Roupas\n4.Sair da loja");
        opt = sc.nextInt();

        while (opt >=1 && opt <= 4) {

            switch (opt) {

                case 1:

                    livros.nome = "Harry Potter";
                    livros.preco = 49.90;
                    System.out.println("Livro disponivel: " + livros.nome + " está por R$" + livros.preco);
                    livros.ler();
                    livros.comprar();
                    break;

                case 2:
                    eletronicos.nome = "Smart TV";
                    eletronicos.preco = 2500.00;
                    System.out.println("Eletrônico disponível: " + eletronicos.nome + " está por R$" + eletronicos.preco);
                    eletronicos.ligar();
                    eletronicos.mexer();
                    eletronicos.desligar();
                    break;

                case 3:
                    roupas.nome = "Casaco de lã";
                    roupas.preco = 499.99;
                    System.out.println("Roupa disponível: " + roupas.nome + " está por R$" + roupas.preco);
                    roupas.olhar();
                    roupas.vestir();

            }

            System.out.println("\nEscolha uma nova categoria ou envie 4 para sair.");
            opt = sc.nextInt();
            if (opt == 4) {
                break;

            }
        }
    }
}