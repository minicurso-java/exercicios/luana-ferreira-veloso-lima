class Livros extends ProdutosMercado {

    public void ler() {
        System.out.println("Ler livro");
    }

    public void comprar() {
        System.out.println("Comprar livro");
    }
}

class Eletronicos extends ProdutosMercado {

    public void ligar() {
        System.out.println("Ligar eletrônico");
    }
    public void mexer() {
        System.out.println("Mexer no eletronico");
    }
    public void desligar() {
        System.out.println("Desligar eletronico");
    }
}

class Roupas extends ProdutosMercado {

    public void olhar() {
        System.out.println("Olhar roupa");
    }
    public void vestir() {
        System.out.println("Vestir roupa");
    }
}
