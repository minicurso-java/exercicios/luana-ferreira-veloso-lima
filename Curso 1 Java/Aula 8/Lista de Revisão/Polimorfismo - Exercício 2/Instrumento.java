class Violao implements InstumentosMusicais {
    public void afinar() {
        System.out.println("Afinando...");
    }
    public void tocar() {
        System.out.println("Don Don");
    }
}
class Piano implements InstumentosMusicais {

    public void afinar() {
        System.out.println("Afinando...");
        }
    public void tocar() {
        System.out.println("Plin Plin");
    }
}
class Flauta implements InstumentosMusicais {

    public void afinar() {
        System.out.println("Afinando...");
    }
    public void tocar() {
        System.out.println("Fuu Fuu");
    }
}