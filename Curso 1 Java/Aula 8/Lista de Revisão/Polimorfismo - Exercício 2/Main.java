import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int opt;

        Violao violao = new Violao();
        Piano piano = new Piano();
        Flauta flauta = new Flauta();

        Scanner sc = new Scanner(System.in);
        System.out.println("Escolha um instrumento:");
        System.out.println("1.Violão\n2.Piano\n3.Flauta\n4.Sair");
        opt = sc.nextInt();

        while (opt >= 1 && opt < 5) {

            switch (opt) {

                case 1:
                    violao.afinar();
                    violao.tocar();
                    break;

                case 2:
                    piano.afinar();
                    piano.tocar();
                    break;

                case 3:
                    flauta.afinar();
                    flauta.tocar();
                    break;

                case 4:
                    break;
            }

            System.out.println("Escolha outro instrumento ou 4 para sair");
            opt = sc.nextInt();
            if (opt == 4)
                break;

        }

    }

}