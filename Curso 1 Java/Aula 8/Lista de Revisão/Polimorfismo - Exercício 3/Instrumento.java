class Carro implements Locomocao {

    public void acelerar() {
        System.out.println("Carro acelerando");
    }

    public void frear() {
        System.out.println("Carro freando");
    }

    public void virar() {
        System.out.println("Carro virando");
    }
}

class Moto implements Locomocao {

    public void acelerar() {
        System.out.println("Moto acelerando");
    }

    public void frear() {
        System.out.println("Moto freando");
    }

    public void virar() {
        System.out.println("Moto virando");
    }

}


class Caminhao implements Locomocao {

    public void acelerar() {
        System.out.println("Caminhao acelerando");
    }

    public void frear() {
        System.out.println("Caminhao freando");
    }

    public void virar() {
        System.out.println("Caminhao virando");
    }

}
