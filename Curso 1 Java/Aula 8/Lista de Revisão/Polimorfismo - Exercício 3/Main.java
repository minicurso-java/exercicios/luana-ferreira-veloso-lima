import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int opt;

        Carro carro = new Carro();
        Moto moto = new Moto();
        Caminhao caminhao = new Caminhao();

        Scanner sc = new Scanner(System.in);
        System.out.println("Escolha um veículo:");
        System.out.println("1.Carro\n2.Moto\n3.Caminhão\n4.Sair");
        opt = sc.nextInt();

        while (opt >= 1 && opt < 5) {

            switch (opt) {

                case 1:
                    carro.acelerar();
                    carro.virar();
                    carro.frear();
                    break;

                case 2:
                    moto.acelerar();
                    moto.virar();
                    moto.frear();
                    break;

                case 3:
                    caminhao.acelerar();
                    caminhao.virar();
                    caminhao.frear();
                    break;

                case 4:
                    break;
            }

            System.out.println("Escolha outro veículo ou 4 para sair");
            opt = sc.nextInt();
            if (opt == 4)
                break;

        }

    }

}