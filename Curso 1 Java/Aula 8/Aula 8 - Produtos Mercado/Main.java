import java.util.Scanner;

public class Main extends Produto {
    public static void main(String[] args) {

        int opt;

        Scanner sc = new Scanner(System.in);
        Produto loja = new Produto();

        System.out.println("\n" + loja.produto);
        System.out.println("Estoque: " + loja.quantidade);

        System.out.println("\n1.Adicionar\n2.Retirar\n3.Valor total\n4.Sair");
        opt = sc.nextInt();

        while (opt >= 1 && opt <= 4) {
            if (opt == 1) {
                loja.adicionar();
            }
            else if (opt == 2) {
                loja.remover();
            }
            else if (opt == 3) {
                loja.valorTotal();
            }
            else {
                break;
            }
            opt = sc.nextInt();
        }
    }
}