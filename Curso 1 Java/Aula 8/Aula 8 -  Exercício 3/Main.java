import entidade.Student;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Student nota = new Student();

        Scanner sc = new Scanner(System.in);

        System.out.println("Aluno:");
        nota.name = sc.nextLine();
        System.out.println("Nota 1: ");
        nota.um = sc.nextDouble();
        System.out.println("Nota 2: ");
        nota.dois = sc.nextDouble();
        System.out.println("Nota 3: ");
        nota.tres = sc.nextDouble();

        nota.media = nota.calcMedia(nota.um, nota.dois, nota.tres);

        System.out.println(nota.name);
        if (nota.media < 60) {
            System.out.println("Não passou!");
            System.out.println("Nota final = " + nota.media);
            System.out.printf("Faltou %.2f pontos", 60 - nota.media);
        }
        else {
            System.out.println("Passou!");
            System.out.println("Nota final = " + nota.media);
        }
    }
}