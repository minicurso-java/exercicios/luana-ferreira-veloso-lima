import java.util.Scanner;

class Quadrado extends FiguraGeometrica {
    double lado, area, perimetro;
    Scanner scan = new Scanner(System.in);
    public double calcularArea() {
        System.out.println("Lado: ");
        lado = scan.nextDouble();
            return area = lado * lado;
    }
    public double calcularPerimetro() {
        return perimetro = lado * 4;
    }
}
class Circulo extends FiguraGeometrica {
    Scanner scan = new Scanner(System.in);
    double raio, area, perimetro;
    public double calcularArea() {
        System.out.println("Raio: ");
        raio = scan.nextDouble();
        return area = raio * raio * Math.PI;
    }
    public double calcularPerimetro() {
        return perimetro = 2 * Math.PI * raio;
    }
}
class Triangulo extends FiguraGeometrica {
    Scanner scan = new Scanner(System.in);
    double lado, altura, area, perimetro;
    public double calcularArea() {
        System.out.println("Lado: ");
        lado = scan.nextDouble();
        System.out.println("Altura: ");
        altura = scan.nextDouble();
        return area = lado * altura / 2;
    }
    public double calcularPerimetro() {
        return perimetro = lado * 3;
    }
}

