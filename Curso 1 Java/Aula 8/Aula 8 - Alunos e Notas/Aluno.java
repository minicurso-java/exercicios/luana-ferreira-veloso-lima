import java.util.Scanner;

public class Aluno {

    Scanner sc = new Scanner(System.in);

    String nome;
    int matricula, disciplinas;

    double media, soma;

    public void calcNotas () {
        System.out.println("Disciplinas: ");
        disciplinas = sc.nextInt();
        double []notas = new double[disciplinas];

        for (int i = 0; i < notas.length; i++) {
            System.out.printf("Disciplina %d: ", (i+1));
            notas[i] = sc.nextDouble();
            soma += notas[i];
        }
        media = soma / notas.length;
    }
}
