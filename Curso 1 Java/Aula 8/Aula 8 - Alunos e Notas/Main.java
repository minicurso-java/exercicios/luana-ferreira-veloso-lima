import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Aluno aluno = new Aluno();

        System.out.println("Aluno: ");
        aluno.nome = sc.nextLine();
        System.out.println("Matrícula: ");
        aluno.matricula = sc.nextInt();

        aluno.calcNotas();

        System.out.println("\nMédia: " + aluno.media);

        if (aluno.media >= 5) {
            System.out.println("\nAluno aprovado");
        } else {
            System.out.println("\nAluno reprovado");
        }
    }
}